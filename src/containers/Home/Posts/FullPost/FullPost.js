import React, {Component} from 'react';
import axiosOrder from "../../../../axiosOrders";
import {Button, Card, CardHeader, CardText, CardTitle} from "reactstrap";
import Moment from "react-moment";
import {NavLink} from "react-router-dom";

class FullPost extends Component {
    state = {
        post: {},
    };

    async componentDidMount() {
        const response = await axiosOrder.get('/posts/' + this.props.match.params.id+ '.json');
        const post = response.data;
        this.setState({post});
    }

    async deletePost() {
        await axiosOrder.delete('/posts/' + this.props.match.params.id + ".json");
        this.props.history.replace('/');
    }

    render() {
        return (
            <div>
                <Card body inverse color="info">
                    <CardHeader>
                        <Moment format="YYYY-MM-DD HH:mm">
                            {this.state.post.time}
                        </Moment>
                    </CardHeader>
                    <CardTitle style={{textAlign: 'center'}}>{this.state.post.title}</CardTitle>
                    <CardText>{this.state.post.text}</CardText>
                    <Button color="secondary" onClick={() =>this.deletePost()}>Delete</Button>
                    <Button tag={NavLink} to={"/posts/" + this.props.match.params.id + "/edit"} exact color="primary">Edit</Button>
                </Card>
            </div>
        );
    }
}
export default FullPost;
