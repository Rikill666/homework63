import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import axiosOrder from "../../axiosOrders";


class Add extends Component {
    state={
        title:"",
        text:"",
    };
    async componentDidMount() {
        if(this.props.match.params.id){
            const response = await axiosOrder.get('/posts/' + this.props.match.params.id+ '.json');
            const post = response.data;
            this.setState({title:post.title, text:post.text});
        }
    }

    valueChanged = event => this.setState({[event.target.name]: event.target.value});
    postHandler = async event => {
        event.preventDefault();
        const post = {
            title: this.state.title,
            text: this.state.text,
            time: new Date()
        };
        if(this.props.match.params.id){
            await axiosOrder.put('/posts/'+ this.props.match.params.id +'.json', post);
            console.log(this.props);
            this.props.history.goBack();
        }
        else{
            await axiosOrder.post('/posts.json', post);
            this.props.history.replace('/');
        }
    };
    render() {
        return (
            <div>
                <Form onSubmit = {this.postHandler}>
                    <FormGroup>
                        <Label for="exampleEmail">Add title</Label>
                        <Input type="text"
                               name="title"
                               id="exampleEmail"
                               onChange = {this.valueChanged}
                               value = {this.state.title}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleText">Add text</Label>
                        <Input type="textarea"
                               name="text"
                               id="exampleText"
                               onChange = {this.valueChanged}
                               value = {this.state.text}
                        />
                    </FormGroup>
                    <Button>Save</Button>
                </Form>
            </div>
        );
    }
}

export default Add;