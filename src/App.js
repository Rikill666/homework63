import React, {Component} from 'react';
import {Container} from "reactstrap";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./containers/Home/Home";
import Contacts from "./containers/Contacts/Contacts";
import About from "./containers/About/About";
import Add from "./containers/Add/Add";
import FullPost from "./containers/Home/Posts/FullPost/FullPost";
import NavMenu from "./components/NavMenu/NavMenu";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Container>
                    <NavMenu/>
                    <Switch>
                        <Route path="/" exact component={Home}/>
                        <Route path="/contacts" component={Contacts}/>
                        <Route path="/about" component={About}/>
                        <Route path="/add" component={Add}/>
                        <Route path="/posts/:id/edit" component={Add}/>
                        <Route path="/posts/:id" component={FullPost}/>
                        <Route render={() => <h1>Not Found</h1>}/>
                    </Switch>
                </Container>
            </BrowserRouter>
        );
    }
}

export default App;