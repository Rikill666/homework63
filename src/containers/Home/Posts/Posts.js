import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import Post from "./Post/Post";
import axiosOrder from "../../../axiosOrders";
import {Button} from "reactstrap";

class Posts extends Component {
    state = {
        posts: {},
    };

    async componentDidMount() {
        const response = await axiosOrder.get('/posts.json');
        const posts = response.data;
        this.setState({posts});
    }

    render() {
        return (
            this.state.posts ?
                <div>
                    {Object.keys(this.state.posts).map(id => (
                        <Post key={id} time={this.state.posts[id].time} title={this.state.posts[id].title}>
                            <Button tag={NavLink} to={'/posts/' + id} exact className="w-25"> >Read more</Button>
                        </Post>
                    ))}
                </div> : <h1>No posts</h1>
        );
    }
}

export default Posts;