import React from 'react';
import {Nav, NavItem, NavLink} from 'reactstrap';
import {NavLink as NavLinkRoute} from "react-router-dom";

const NavMenu = (props) => {
    {
        return (
            <Nav>
                <NavItem>
                    <NavLink tag={NavLinkRoute} to="/" exact className="nav-link">Home</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={NavLinkRoute} to="/add" exact className="nav-link">Add</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={NavLinkRoute} to="/about" exact className="nav-link">About</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={NavLinkRoute} to="/contacts" exact className="nav-link">Contacts</NavLink>
                </NavItem>
            </Nav>
        );
    }
};

export default NavMenu;